package adamlab.mime.data.analysis.definitions;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class Session implements Serializable
{
	private static final long serialVersionUID = 1226054491894969016L;
	
	public UUID ID;
	public Date timestamp;
	public Date[] timestamps;
	
	public UUID getID()
	{
		return ID;
	}
	
	public void setID(UUID iD)
	{
		ID = iD;
	}
	
	public Date getTimestamp()
	{
		return timestamp;
	}
	
	public void setTimestamp(Date timestamp)
	{
		this.timestamp = timestamp;
	}
	
	public Date[] getTimestamps()
	{
		return timestamps;
	}
	
	public void setTimestamps(Date[] timestamps)
	{
		this.timestamps = timestamps;
	}
}
