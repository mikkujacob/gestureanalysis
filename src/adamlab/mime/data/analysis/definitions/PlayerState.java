package adamlab.mime.data.analysis.definitions;

import java.io.Serializable;
import java.util.Date;

public class PlayerState implements Serializable
{
	private static final long serialVersionUID = -2493043778441902433L;
	
	public Date timestamp;
	public Vector3 headPosition;
	public Quaternion headRotation;
	public Vector3 leftHandPosition;
	public Quaternion leftHandRotation;
	public Vector3 rightHandPosition;
	public Quaternion rightHandRotation;
	public Vector3 objectPosition;
	public Quaternion objectRotation;
	public Boolean leftControllerGripButtonDown;
	public Boolean leftControllerTriggerButtonDown;
	public Boolean rightControllerGripButtonDown;
	public Boolean rightControllerTriggerButtonDown;
	public Boolean leftControllerGripButtonUp;
	public Boolean leftControllerTriggerButtonUp;
	public Boolean rightControllerGripButtonUp;
	public Boolean rightControllerTriggerButtonUp;
	public Vector3[] skeletalPositions; //Joints: [root, pelvis, spine, chest, neck, head, leftShoulder, leftUpperArm, leftForearm, leftHand, rightShoulder, rightUpperArm, rightForearm, rightHand, leftThigh, leftCalf, leftFoot, leftToes, rightThigh, rightCalf, rightFoot, rightToes]
	public Quaternion[] skeletalRotations; //Joints: [root, pelvis, spine, chest, neck, head, leftShoulder, leftUpperArm, leftForearm, leftHand, rightShoulder, rightUpperArm, rightForearm, rightHand, leftThigh, leftCalf, leftFoot, leftToes, rightThigh, rightCalf, rightFoot, rightToes]
	
	public PlayerState()
	{
		timestamp = new Date();
		headPosition = new Vector3();
		headRotation = new Quaternion();
		leftHandPosition = new Vector3();
		leftHandRotation = new Quaternion();
		rightHandPosition = new Vector3();
		rightHandRotation = new Quaternion();
		objectPosition = new Vector3();
		objectRotation = new Quaternion();
		leftControllerGripButtonDown = new Boolean(false);
		leftControllerTriggerButtonDown = new Boolean(false);
		rightControllerGripButtonDown = new Boolean(false);
		rightControllerTriggerButtonDown = new Boolean(false);
		leftControllerGripButtonUp = new Boolean(false);
		leftControllerTriggerButtonUp = new Boolean(false);
		rightControllerGripButtonUp = new Boolean(false);
		rightControllerTriggerButtonUp = new Boolean(false);
		skeletalPositions = new Vector3[22];
		for(int i = 0; i < skeletalPositions.length; i++)
		{
			skeletalPositions[i] = new Vector3();
		}
		skeletalRotations = new Quaternion[22];
		for(int i = 0; i < skeletalRotations.length; i++)
		{
			skeletalRotations[i] = new Quaternion();
		}
	}
	
	public PlayerState(Date timestamp, Vector3 headPosition, Quaternion headRotation,
			Vector3 leftHandPosition, Quaternion leftHandRotation,
			Vector3 rightHandPosition, Quaternion rightHandRotation,
			Vector3 objectPosition, Quaternion objectRotation,
			Boolean leftControllerGripButtonDown, Boolean leftControllerTriggerButtonDown,
			Boolean rightControllerGripButtonDown, Boolean rightControllerTriggerButtonDown,
			Boolean leftControllerGripButtonUp, Boolean leftControllerTriggerButtonUp,
			Boolean rightControllerGripButtonUp, Boolean rightControllerTriggerButtonUp,
			Vector3[] skeletalPositions, Quaternion[] skeletalRotations)
	{
		this.timestamp = timestamp;
		this.headPosition = new Vector3(headPosition);
		this.headRotation = new Quaternion(headRotation);
		this.leftHandPosition = new Vector3(leftHandPosition);
		this.leftHandRotation = new Quaternion(leftHandRotation);
		this.rightHandPosition = new Vector3(rightHandPosition);
		this.rightHandRotation = new Quaternion(rightHandRotation);
		this.objectPosition = new Vector3(objectPosition);
		this.objectRotation = new Quaternion(objectRotation);
		this.leftControllerGripButtonDown = new Boolean(leftControllerGripButtonDown);
		this.leftControllerTriggerButtonDown = new Boolean(leftControllerTriggerButtonDown);
		this.rightControllerGripButtonDown = new Boolean(rightControllerGripButtonDown);
		this.rightControllerTriggerButtonDown = new Boolean(rightControllerTriggerButtonDown);
		this.leftControllerGripButtonUp = new Boolean(leftControllerGripButtonUp);
		this.leftControllerTriggerButtonUp = new Boolean(leftControllerTriggerButtonUp);
		this.rightControllerGripButtonUp = new Boolean(rightControllerGripButtonUp);
		this.rightControllerTriggerButtonUp = new Boolean(rightControllerTriggerButtonUp);
		this.skeletalPositions = new Vector3[22];
		for(int i = 0; i < skeletalPositions.length; i++)
		{
			this.skeletalPositions[i] = new Vector3(skeletalPositions[i]);
		}
		this.skeletalRotations = new Quaternion[22];
		for(int i = 0; i < skeletalRotations.length; i++)
		{
			this.skeletalRotations[i] = new Quaternion(skeletalRotations[i]);
		}
	}
	
	public PlayerState(PlayerState state)
	{
		this(state.timestamp, state.headPosition, state.headRotation, 
				state.leftHandPosition, state.leftHandRotation, 
				state.rightHandPosition, state.rightHandRotation, 
				state.objectPosition, state.objectRotation,
				state.leftControllerGripButtonDown, state.leftControllerTriggerButtonDown, 
				state.rightControllerGripButtonDown, state.rightControllerTriggerButtonDown, 
				state.leftControllerGripButtonUp, state.leftControllerTriggerButtonUp, 
				state.rightControllerGripButtonUp, state.rightControllerTriggerButtonUp, 
				state.skeletalPositions, state.skeletalRotations);
	}
	
	public PlayerState clone()
	{
		return new PlayerState(this);
	}
	
	public static PlayerState interpolate(PlayerState a, PlayerState b, double amount)
	{
		PlayerState result = new PlayerState();
		result.timestamp.setTime(Math.round(a.timestamp.getTime() + (b.timestamp.getTime() - a.timestamp.getTime()) * amount));;
		result.headPosition = Vector3.interpolate(a.headPosition, b.headPosition, amount);
		result.headRotation = Quaternion.interpolate(a.headRotation, b.headRotation, amount);
		result.leftHandPosition = Vector3.interpolate(a.leftHandPosition, b.leftHandPosition, amount);
		result.leftHandRotation = Quaternion.interpolate(a.leftHandRotation, b.leftHandRotation, amount);
		result.rightHandPosition = Vector3.interpolate(a.rightHandPosition, b.rightHandPosition, amount);
		result.rightHandRotation = Quaternion.interpolate(a.rightHandRotation, b.rightHandRotation, amount);
		result.objectPosition = Vector3.interpolate(a.objectPosition, b.objectPosition, amount);
		result.objectRotation = Quaternion.interpolate(a.objectRotation, b.objectRotation, amount);
		result.leftControllerGripButtonDown = new Boolean((amount < 0.5) ? a.leftControllerGripButtonDown : b.leftControllerGripButtonDown);
		result.leftControllerTriggerButtonDown = new Boolean((amount < 0.5) ? a.leftControllerTriggerButtonDown : b.leftControllerTriggerButtonDown);
		result.rightControllerGripButtonDown = new Boolean((amount < 0.5) ? a.rightControllerGripButtonDown : b.rightControllerGripButtonDown);
		result.rightControllerTriggerButtonDown = new Boolean((amount < 0.5) ? a.rightControllerTriggerButtonDown : b.rightControllerTriggerButtonDown);
		result.leftControllerGripButtonUp = new Boolean((amount < 0.5) ? a.leftControllerGripButtonUp : b.leftControllerGripButtonUp);
		result.leftControllerTriggerButtonUp = new Boolean((amount < 0.5) ? a.leftControllerTriggerButtonUp : b.leftControllerTriggerButtonUp);
		result.rightControllerGripButtonUp = new Boolean((amount < 0.5) ? a.rightControllerGripButtonUp : b.rightControllerGripButtonUp);
		result.rightControllerTriggerButtonUp = new Boolean((amount < 0.5) ? a.rightControllerTriggerButtonUp : b.rightControllerTriggerButtonUp);
		result.skeletalPositions = new Vector3[22];
		for(int i = 0; i < 22; i++)
		{
			result.skeletalPositions[i] = Vector3.interpolate(a.skeletalPositions[i], b.skeletalPositions[i], amount);
		}
		result.skeletalRotations = new Quaternion[22];
		for(int i = 0; i < 22; i++)
		{
			result.skeletalRotations[i] = Quaternion.interpolate(a.skeletalRotations[i], b.skeletalRotations[i], amount);
		}
		return result;
	}
	
	public String toString()
	{
		return "Timestamp: " + timestamp.toString() + 
				", Head Position: " + headPosition.toString() + 
				", Head Rotation: " + headRotation.toString() + 
				", Left Controller Grip Button Down: " + leftControllerGripButtonDown.toString() + 
				", Skeletal Positions: " + toString(skeletalPositions) + 
				", Skeletal Rotations: " + toString(skeletalRotations);
	}
	
	public <E> String toString(E[] array)
	{
		String value = "[";
		for(E element : array)
		{
			value += element.toString() + ", ";
		}
		return value.substring(0, value.length() - 2) + "]";
	}

	public Date getTimestamp()
	{
		return timestamp;
	}

	public void setTimestamp(Date timestamp)
	{
		this.timestamp = timestamp;
	}

	public Vector3 getHeadPosition()
	{
		return headPosition;
	}

	public void setHeadPosition(Vector3 headPosition)
	{
		this.headPosition = headPosition;
	}

	public Quaternion getHeadRotation()
	{
		return headRotation;
	}

	public void setHeadRotation(Quaternion headRotation)
	{
		this.headRotation = headRotation;
	}

	public Vector3 getLeftHandPosition()
	{
		return leftHandPosition;
	}

	public void setLeftHandPosition(Vector3 leftHandPosition)
	{
		this.leftHandPosition = leftHandPosition;
	}

	public Quaternion getLeftHandRotation()
	{
		return leftHandRotation;
	}

	public void setLeftHandRotation(Quaternion leftHandRotation)
	{
		this.leftHandRotation = leftHandRotation;
	}

	public Vector3 getRightHandPosition()
	{
		return rightHandPosition;
	}

	public void setRightHandPosition(Vector3 rightHandPosition)
	{
		this.rightHandPosition = rightHandPosition;
	}

	public Quaternion getRightHandRotation()
	{
		return rightHandRotation;
	}

	public void setRightHandRotation(Quaternion rightHandRotation)
	{
		this.rightHandRotation = rightHandRotation;
	}
	
	public Vector3 getObjectPosition() 
	{
		return objectPosition;
	}

	public void setObjectPosition(Vector3 objectPosition) 
	{
		this.objectPosition = objectPosition;
	}

	public Quaternion getObjectRotation() 
	{
		return objectRotation;
	}

	public void setObjectRotation(Quaternion objectRotation) 
	{
		this.objectRotation = objectRotation;
	}

	public Boolean getLeftControllerGripButtonDown()
	{
		return leftControllerGripButtonDown;
	}

	public void setLeftControllerGripButtonDown(Boolean leftControllerGripButtonDown)
	{
		this.leftControllerGripButtonDown = leftControllerGripButtonDown;
	}

	public Boolean getLeftControllerTriggerButtonDown()
	{
		return leftControllerTriggerButtonDown;
	}

	public void setLeftControllerTriggerButtonDown(Boolean leftControllerTriggerButtonDown)
	{
		this.leftControllerTriggerButtonDown = leftControllerTriggerButtonDown;
	}

	public Boolean getRightControllerGripButtonDown()
	{
		return rightControllerGripButtonDown;
	}

	public void setRightControllerGripButtonDown(Boolean rightControllerGripButtonDown)
	{
		this.rightControllerGripButtonDown = rightControllerGripButtonDown;
	}

	public Boolean getRightControllerTriggerButtonDown()
	{
		return rightControllerTriggerButtonDown;
	}

	public void setRightControllerTriggerButtonDown(Boolean rightControllerTriggerButtonDown)
	{
		this.rightControllerTriggerButtonDown = rightControllerTriggerButtonDown;
	}

	public Boolean getLeftControllerGripButtonUp()
	{
		return leftControllerGripButtonUp;
	}

	public void setLeftControllerGripButtonUp(Boolean leftControllerGripButtonUp)
	{
		this.leftControllerGripButtonUp = leftControllerGripButtonUp;
	}

	public Boolean getLeftControllerTriggerButtonUp()
	{
		return leftControllerTriggerButtonUp;
	}

	public void setLeftControllerTriggerButtonUp(Boolean leftControllerTriggerButtonUp)
	{
		this.leftControllerTriggerButtonUp = leftControllerTriggerButtonUp;
	}

	public Boolean getRightControllerGripButtonUp()
	{
		return rightControllerGripButtonUp;
	}

	public void setRightControllerGripButtonUp(Boolean rightControllerGripButtonUp)
	{
		this.rightControllerGripButtonUp = rightControllerGripButtonUp;
	}

	public Boolean getRightControllerTriggerButtonUp()
	{
		return rightControllerTriggerButtonUp;
	}

	public void setRightControllerTriggerButtonUp(Boolean rightControllerTriggerButtonUp)
	{
		this.rightControllerTriggerButtonUp = rightControllerTriggerButtonUp;
	}

	public Vector3[] getSkeletalPositions()
	{
		return skeletalPositions;
	}

	public void setSkeletalPositions(Vector3[] skeletalPositions)
	{
		this.skeletalPositions = skeletalPositions;
	}

	public Quaternion[] getSkeletalRotations()
	{
		return skeletalRotations;
	}

	public void setSkeletalRotations(Quaternion[] skeletalRotations)
	{
		this.skeletalRotations = skeletalRotations;
	}
}
