package adamlab.mime.data.analysis.definitions;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

// records body positions at certain times and generate body positions
// at intermediate times by interpolation
public class GestureInterpolator implements java.io.Serializable
{

	// serial ID of LeaderRecord
	private static final long serialVersionUID = 8077118670358540738L;

	// tree map storing body positions and the time this body has been recorded
	private TreeMap<Long, PlayerState> states = new TreeMap<Long, PlayerState>();

	// relative acceleration of the replay against the original scene
	private double replaySpeed = 1f;

	// whether the replay has to be repeated or not
	// if yes, it is repeated backward once finished until it is back to the
	// beginning
	private boolean isRepetitionOn = false;

	// if true, always repeat from the beginning to the end
	// if false, play from the beginning to the end, then from the end to the
	// beginning, then from the beginning to the end, etc.
	private boolean isDirectRepetition = false;

	private long duration = 0;

	// total time elapsed since the beginning of the recording or the replay
	private long totalTime = 0;

	private double percentPassed = 0.0;

	public GestureInterpolator(ArrayList<PlayerState> states)
	{
		if (states.size() != 0)
		{
			long startingTime = states.get(0).getTimestamp().getTime();

			for (PlayerState state : states)
			{
				this.states.put((state.getTimestamp().getTime()) - startingTime, state);
			}

			duration = states.get(states.size() - 1).getTimestamp().getTime() - states.get(0).getTimestamp().getTime();
		}
	}

	public void setReplaySpeed(double replaySpeed)
	{
		this.replaySpeed = replaySpeed;
	}

	public double getReplaySpeed()
	{
		return this.replaySpeed;
	}

	// updates whether the replay has to be repeated or not
	public void setIsRepetitionOn(boolean isRepetitionOn)
	{
		this.isRepetitionOn = isRepetitionOn;
	}

	public void setDirectRepetition(boolean isDirectRepetition)
	{
		this.isDirectRepetition = isDirectRepetition;
	}

	public boolean isRepetitionOn()
	{
		return this.isRepetitionOn;
	}

	public boolean isDirectRepetition()
	{
		return this.isDirectRepetition;
	}

	// sets the speed back to its original value
	public void reinitializeSpeed()
	{
		replaySpeed = 1;
	}

	//Returns the position of the body (real or interpolated) at this time in millis.
	public PlayerState at(long time)
	{
		if (time >= states.lastKey())
		{
			return states.lastEntry().getValue().clone();
		}
		else if (time <= states.firstKey())
		{
			return states.firstEntry().getValue().clone();
		}
		else
		{
			Map.Entry<Long, PlayerState> floorEntry = states.floorEntry(time);
			long floorTime = floorEntry.getKey();
			PlayerState floorState = floorEntry.getValue();
			if (time == floorTime)
			{
				return floorState.clone();
			}
			Map.Entry<Long, PlayerState> ceilingEntry = states.ceilingEntry(time);
			long ceilingTime = ceilingEntry.getKey();
			PlayerState ceilingState = ceilingEntry.getValue();
			return PlayerState.interpolate(floorState, ceilingState, ((double)(time - floorTime) / (double)(ceilingTime - floorTime)));
		}
	}

	//Returns the position of the body (real or interpolated) after a time from now equal to deltaTime millis.
	public PlayerState replayFrame(long deltaTime)
	{
		totalTime += replaySpeed * deltaTime;
		long replay_time = getReplayTime();

		percentPassed = replay_time / duration;

		// System.out.println(totalTime);

		PlayerState response = at(replay_time);
//		response.timestamp.setTime(replay_time);
		
		// the position of the body corresponding to the given time is returned
		return response;
	}

	public PlayerState atFraction(double fraction)
	{
		long time = Math.round(fraction * duration);
		return at(time);
	}

	private long getReplayTime()
	{
		if (!isRepetitionOn)
		{
			if (totalTime > duration)
			{
				return duration;
			}
			else
			{
				return totalTime;
			}
		}
		else
		{
			int nb_cycles = (int) (totalTime / duration);
			long endOfLastCycle = duration * nb_cycles;
			long timeInCurrentCycle = totalTime - endOfLastCycle;
			if (isDirectRepetition || nb_cycles % 2 == 0)
			{
				return timeInCurrentCycle;
			}
			else
			{
				return duration - timeInCurrentCycle;
			}
		}
	}

	public double percentPassed()
	{
		return percentPassed;
	}

	public long getBasicDuration()
	{
		return states.lastKey();
	}

	public long getTimePlayed()
	{
		return totalTime;
	}

	public long getTimeInCurrentCycle()
	{
		int nb_cycles = (int) (totalTime / duration);
		long endOfLastCycle = duration * nb_cycles;
		long timeInCurrentCycle = totalTime - endOfLastCycle;
		return timeInCurrentCycle;
	}
}
