package adamlab.mime.data.analysis.definitions;

import java.io.Serializable;

public class Vector3 implements Serializable
{
	private static final long serialVersionUID = 7920382011067732651L;
	
	public double x;
	public double y;
	public double z;
	
	public Vector3()
	{
		x = y = z = 0.0;
	}
	
	public Vector3(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vector3(Vector3 copy)
	{
		this.x = copy.x;
		this.y = copy.y;
		this.z = copy.z;
	}
	
	public static Vector3 interpolate(Vector3 a, Vector3 b, double amount)
	{
		return new Vector3(a.x + (b.x - a.x) * amount, a.y + (b.y - a.y) * amount, a.z + (b.z - a.z) * amount);
	}
	
	public String toString()
	{
		return "{\"x\":" + x + ",\"y\":" + y + ",\"z\":" + z + "}";
	}

	public double getX()
	{
		return x;
	}

	public void setX(double x)
	{
		this.x = x;
	}

	public double getY()
	{
		return y;
	}

	public void setY(double y)
	{
		this.y = y;
	}

	public double getZ()
	{
		return z;
	}

	public void setZ(double z)
	{
		this.z = z;
	}
}
