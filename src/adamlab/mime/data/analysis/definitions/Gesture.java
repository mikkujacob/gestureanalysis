package adamlab.mime.data.analysis.definitions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class Gesture implements Serializable
{
	private static final long serialVersionUID = -2954319394308161297L;
	
	public UUID ID;
	public Date timestamp;
	public String objectName;
	public String objectSize;
	public Vector3 objectPosition;
	public Quaternion objectRotation;
	public ArrayList<PlayerState> playerStates;
	
	public Gesture()
	{
		ID = UUID.randomUUID();
		timestamp = new Date();
		objectName = "";
		objectSize = "";
		objectPosition = new Vector3();
		objectRotation = new Quaternion();
		playerStates = new ArrayList<PlayerState>();
	}
	
	public Gesture(UUID ID, Date timestamp, String objectName, String objectSize, 
			Vector3 objectPosition, Quaternion objectRotation, ArrayList<PlayerState> playerStates)
	{
		this.ID = ID;
		this.timestamp = timestamp;
		this.objectName = objectName;
		this.objectSize = objectSize;
		this.objectPosition = new Vector3(objectPosition);
		this.objectRotation = new Quaternion(objectRotation);
		this.playerStates = new ArrayList<PlayerState>(playerStates);
	}
	
	public Gesture(Gesture gesture)
	{
		this.ID = gesture.ID;
		this.objectName = gesture.objectName;
		this.objectSize = gesture.objectSize;
		this.objectPosition = new Vector3(gesture.objectPosition);
		this.objectRotation = new Quaternion(gesture.objectRotation);
		this.playerStates = new ArrayList<PlayerState>(gesture.playerStates);
	}
	
	public long getDurationMillis()
	{
		return (playerStates.get(playerStates.size() - 1).timestamp.getTime() - playerStates.get(0).timestamp.getTime());
	}
	
	public double getDuration()
	{
		return getDurationMillis() / 1000.0;
	}
	
	public String toString()
	{
		return "UUID: " + ID.toString() + 
				", Object Name: " + objectName + 
				", Object Size: " + objectSize + 
				", Object Position: " + objectPosition.toString() + 
				", Object Rotation: " + objectRotation.toString() + 
				", PlayerStates[0]: " + ((!playerStates.isEmpty()) ? playerStates.get(0).toString() : "");
	}
	
	public UUID getID()
	{
		return ID;
	}
	
	public void setID(UUID iD)
	{
		ID = iD;
	}
	
	public Date getTimestamp()
	{
		return timestamp;
	}
	
	public void setTimestamp(Date timestamp)
	{
		this.timestamp = timestamp;
	}
	
	public String getObjectName()
	{
		return objectName;
	}
	
	public void setObjectName(String objectName)
	{
		this.objectName = objectName;
	}
	
	public String getObjectSize()
	{
		return objectSize;
	}
	
	public void setObjectSize(String objectSize)
	{
		this.objectSize = objectSize;
	}
	
	public Vector3 getObjectPosition()
	{
		return objectPosition;
	}
	
	public void setObjectPosition(Vector3 objectPosition)
	{
		this.objectPosition = objectPosition;
	}
	
	public Quaternion getObjectRotation()
	{
		return objectRotation;
	}
	
	public void setObjectRotation(Quaternion objectRotation)
	{
		this.objectRotation = objectRotation;
	}

	public ArrayList<PlayerState> getPlayerStates()
	{
		return playerStates;
	}

	public void setPlayerStates(ArrayList<PlayerState> playerStates)
	{
		this.playerStates = playerStates;
	}
}
