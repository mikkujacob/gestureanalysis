package adamlab.mime.data.analysis.definitions;

import java.io.Serializable;

public class Quaternion implements Serializable
{
	private static final long serialVersionUID = 6160021390939693449L;

	public double x;
	public double y;
	public double z;
	public double w;

	public Quaternion()
	{
		x = y = z = 0.0;
		w = 1;
	}

	public Quaternion(double x, double y, double z, double w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	public Quaternion(Quaternion copy)
	{
		this.x = copy.x;
		this.y = copy.y;
		this.z = copy.z;
		this.w = copy.w;
	}

	public double norm()
	{
		return Math.sqrt(w * w + x * x + y * y + z * z);
	}

	public Quaternion normalize()
	{
		return new Quaternion(x / this.norm(), y / this.norm(), z / this.norm(), w / this.norm());
	}

	public static double dotProduct(Quaternion a, Quaternion b)
	{
		return a.w * b.w + a.x * b.x + a.y * b.y + a.z * b.z;
	}

	public Quaternion reverse()
	{
		return new Quaternion(-x, -y, -z, w);
	}
	
	public Quaternion multiplyScalar(double scalar)
	{
		return new Quaternion(x * scalar, y * scalar, z * scalar, w * scalar);
	}
	
	public static Quaternion add(Quaternion a, Quaternion b)
	{
		return new Quaternion(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
	}

	public static Quaternion interpolate(Quaternion a, Quaternion b, double amount)
	{
		
	    // Only unit quaternions are valid rotations.
	    // Normalize to avoid undefined behavior.
	    a.normalize();
	    b.normalize();

	    // Compute the cosine of the angle between the two vectors.
	    double dot = dotProduct(a, b);

	    // If the dot product is negative, the quaternions
	    // have opposite handed-ness and slerp won't take
	    // the shorter path. Fix by reversing one quaternion.
	    if (dot < 0.0f) {
	        b = b.reverse();
	        dot = -dot;
	    }  

	    final double DOT_THRESHOLD = 0.9995;
	    if (dot > DOT_THRESHOLD) 
	    {
	        // If the inputs are too close for comfort, linearly interpolate
	        // and normalize the result.

	        Quaternion result = Quaternion.lerp(a, b, amount);
	        return result.normalize();
	    }
	    
	    dot = Quaternion.clamp(dot, -1, 1);           // Robustness: Stay within domain of acos()
	    double theta_0 = Math.acos(dot);  // theta_0 = angle between input vectors
	    double theta = theta_0 * amount;  // theta = angle between v0 and result

	    double s0 = Math.cos(theta) - dot * Math.sin(theta) / Math.sin(theta_0);  // == sin(theta_0 - theta) / sin(theta_0)
	    double s1 = Math.sin(theta) / Math.sin(theta_0);

	    Quaternion result = Quaternion.add(a.multiplyScalar(s0), b.multiplyScalar(s1));
	    return result.normalize();
	}
	
	public static double clamp(double x, double min, double max)
	{
		if(x < min)
		{
			return min;
		}
		else if(x > max)
		{
			return max;
		}
		
		return x;
	}

	public static Quaternion lerp(Quaternion a, Quaternion b, double amount)
	{
		return new Quaternion(a.x + (b.x - a.x) * amount, a.y + (b.y - a.y) * amount, a.z + (b.z - a.z) * amount, a.w + (b.w - a.w) * amount);
	}

	public String toString()
	{
		return "{\"x\":" + x + ",\"y\":" + y + ",\"z\":" + z + ",\"w\":" + w + "}";
	}

	public double getX()
	{
		return x;
	}

	public void setX(double x)
	{
		this.x = x;
	}

	public double getY()
	{
		return y;
	}

	public void setY(double y)
	{
		this.y = y;
	}

	public double getZ()
	{
		return z;
	}

	public void setZ(double z)
	{
		this.z = z;
	}

	public double getW()
	{
		return w;
	}

	public void setW(double w)
	{
		this.w = w;
	}
}
