package adamlab.mime.data.analysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import com.dtw.TimeWarpInfo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.timeseries.TimeSeries;
import com.timeseries.TimeSeriesPoint;
import com.util.DistanceFunction;
import com.util.DistanceFunctionFactory;

import adamlab.mime.data.analysis.definitions.Gesture;
import adamlab.mime.data.analysis.definitions.GestureInterpolator;
import adamlab.mime.data.analysis.definitions.PlayerState;
import adamlab.mime.data.analysis.definitions.Quaternion;
import adamlab.mime.data.analysis.definitions.Vector3;
import adamlab.mime.data.analysis.utils.HilbertCurveCoordinateConversionUtils;
import net.imglib2.RandomAccess;
import net.imglib2.exception.ImgLibException;
import net.imglib2.img.Img;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.script.ImgLib;
import net.imglib2.type.numeric.integer.UnsignedByteType;

/**
 * @author Mikhail Jacob
 * Class for analyzing gestures from data collection studies and convert between gesture files and datasets and vice versa.
 */
public class GestureAnalysis
{
	public static final String grandParentFolderPath = "/data/VRData/New_Data_Annotated/";
	public static final String[] parentFolderPaths = new String[]
	{ "01_new/", "02_new/", "03_new/", "04_new/", "05_new/", "06_new/", "07_new/", "08_new/", "09_new/", "10_new/", "11_new/", "12_new/", "13_new/", "14_new/" };
	public static String[] gestureFileNames;
	public static final String cleanedGestureFileNames = "AllGestureFileNamesRelative-900-3.33-10.0-2018-09-21-13-51-02-887.filenames";
	public static final String cleanedTimeSeriesListFileName = "CleanedTimeSeriesList-329-0.5-10.5-2018-03-12-16-55-02-032.timeseries";
	public static final String generatedImageFolder = "GeneratedImages/";
	public static final String imageCSVFolder = "ImageCSVs/";
	public static final String originalTimeSeriesListName = "CleanedTimeSeriesListOriginal-329-0.5-10.5-2018-03-15-12-41-11-913.timeseries";
	public static final String generatedTimeSeriesListName = "CleanedTimeSeriesListGenerated-329-0.5-10.5-2018-03-15-12-41-11-913.timeseries";
	public static final String deepIMAGINATIONTimeSeriesPath = "DeepIMAGINATIONTimeSeries/";
	public static final String originalGesturePath = "original/";
	public static final String generatedGesturePath = "generated/";
	public static final String propAttributesSummariesFileName = "prop-attributes-summaries.csv";
	public static final String[] affordanceFields = new String[] {"Objects", "Summary-ID", "Shape.cuboidal", "Shape.cylindrical", "Shape.ellipsoidal", "Shape.spherical", "Shape.conical", "Shape.toroidal", "Shape.helical", "Size.large", "Size.medium", "Size.small", "Thickness.torso-thick", "Thickness.head-thick", "Thickness.fist-thick", "Thickness.thin", "Flatness.flat", "Concavity.deep", "Concavity.shallow", "Taper", "Rigidity", "Curvature", "Hole-Size.finger-sized", "Hole-Size.fist-sized", "Hole-Size.head-leg-sized", "Hole-Size.torso-sized", "digits-symbols-characters"};
	public static final String[] timeSeriesConversionParams = new String[] {"CleanedTimeSeriesList$-%-^-329-0.5-10.5-2018-03-27-16-22-22-317.timeseries", "^/"};
	public static String[] propNames = new String[] {"Squid w Tentacles Partless", "Lolliop w Cube Ends Partless", "Giant U w Flat Base Partless", "Big Curved Axe Partless", "Sunflower Partless", "Air Horn", "Corn Dog", "Giant Electric Plug", "Ring w Tail End", "Ring w Inward Spokes", "Large Ring w Outward Spokes", "Horseshoe", "Tube w Cones", "Thin Stick w Hammer End", "Helix", "Giant Golf T", "Circle With Ring", "Palm Tree Partless", "Ladle", "Plunger"};
	public static final String propNamesFileName = "prop-names.csv";
	public static final boolean isAnnotatedGestureOnly = true;
	public static final String[] parentAnnotatedFolderPaths = new String[] {"01A/", "02A/", "03A/", "01L/", "02L/", "03L/", "01W/", "02W/", "03W/", "01N/", "02N/", "03N/", "04A/", "04L/", "04N/", "05A/", "05L/", "05N/"};
		
	public static int millisecondsPerFrame = 32;
	public static ArrayList<String> originalImageFileNames = new ArrayList<String>();
	public static ArrayList<String> generatedImageFileNames = new ArrayList<String>();
	public static int imageCSVRows = 150;//TODO based on 27000 dimensions
	public static int imageCSVcolumns = 180;
	public static int imageCSVpadding = 0;

	public static Gson gson;
	
	public static ArrayList<String> allGestureFileNameList = new ArrayList<String>();
	public static HashMap<String, Integer> objectNameGestureCount = new HashMap<String, Integer>();
	public static HashMap<String, Integer> objectNameSizeGestureCount = new HashMap<String, Integer>();
	public static HashMap<String, Integer> affordanceFeatureGestureCount = new HashMap<String, Integer>();
	public static HashMap<String, HashMap<String, Integer>> objectAffordances = new HashMap<String, HashMap<String,Integer>>();
	public static ArrayList<Gesture> gestureList = new ArrayList<Gesture>();
	public static ArrayList<String> cleanedGestureFileNameList = new ArrayList<String>();
	public static ArrayList<TimeSeries> timeSeriesList = new ArrayList<TimeSeries>();
	public static ArrayList<String> affordanceList = new ArrayList<String>();
	public static ArrayList<String> pretendObjectList = new ArrayList<String>();
	public static ArrayList<String> pretendActionList = new ArrayList<String>();

	public static final double gestureDurationLowerBound = 3.33;
	public static final double gestureDurationUpperBound = 10;
	public static final int numberOfSamplesPerGesture = 900;
	
	public static final String distanceFunctionName = "EuclideanDistance";
	public static final int radius = 25;
	
	public static final int imageSetSize = 800;
	public static final boolean isImageGeneration = true;
	public static final boolean isPaddedToSquare = true;
	
	public enum TimeSeriesType
	{
		POSITION12,//Position data ONLY with 4 joints
		ORIENTATION16,//Rotation data ONLY with 4 joints
		BOTH28,//Position and rotation data with 4 joints
		BOTHWITHGRABS30,//Position and rotation data with 2 grab buttons status
		FOURJOINTWITHOBJECT//Position and rotation data with 4 joints and 1 object
	}

	public static void main(String[] args)
	{
		System.out.println("Experiment Started... " + new Date());
		
		//GENERATE DATA FOR DeepIMAGINATION
//		readFoldersAndCreateFileList(); //STEP 1. if generating dataset.
//		readGesturesFromFileListAndProcess(false, true, true, true, 0, "0", TimeSeriesType.BOTHWITHGRABS30, numberOfSamplesPerGesture); //STEP 2, if generating dataset.
		
		//INTERPRET DATA FROM DeepIMAGINATION
//		readImageCSVsAndGenerateTimeSeries(false); // STEP 1 then copy timeseries files over to deepIMAGINATIONTimeSeriesPath location
		
//		// DO ALL THE STEPS BELOW FOR STEP 2.
//		boolean remap = true; // STEP 2
//		for(int i = 0; i < propNames.length; i++)
//		{
//			String prop = propNames[i];
//			readTimeSeriesListsAndGenerateGestures(timeSeriesConversionParams[0].replaceFirst("\\$", "Original").replaceFirst("%", i + "").replaceFirst("\\^", prop), "Original " + timeSeriesConversionParams[1].replaceFirst("\\^", prop), timeSeriesConversionParams[0].replaceFirst("\\$", "Generated").replaceFirst("%", i + "").replaceFirst("\\^", prop), "Generated " + timeSeriesConversionParams[1].replaceFirst("\\^", prop), remap); // STEP 2
//		}
//		readTimeSeriesListsAndGenerateGestures(remap); // STEP 2
		
//		String[] params = new String[propNames.length * 2]; // STEP 3
//		for(int i = 0; i < propNames.length; i++)
//		{
//			params[2 * i + 0] = "Original " + propNames[i] + "/"; // STEP 3
//			params[2 * i + 1] = "Generated " + propNames[i] + "/"; // STEP 3
//		}
//		readFilesAndGenerateFilenameList(params); // STEP 3
//		readFilesAndGenerateFilenameList(new String[] {originalGesturePath, generatedGesturePath}); // STEP 3
		
		System.out.println("Experiment Complete..." + new Date());
	}
	
	public static void readFilesAndGenerateFilenameList(String[] parentFolderPaths)
	{
		String grandParentPath = new File(".").getAbsolutePath();
		grandParentPath = grandParentPath.substring(0, grandParentPath.length() - 2) + grandParentFolderPath + deepIMAGINATIONTimeSeriesPath;

		String folderPath = "";
		for (String parentFolderPath : parentFolderPaths)
		{
			System.out.println("Folder: " + parentFolderPath);
			folderPath = grandParentPath + parentFolderPath;
			File folder = new File(folderPath);
			String[] gestureFileNames = folder.list(new FilenameFilter()
			{
				@Override
				public boolean accept(File dir, String name)
				{
					return name.endsWith(".gesture");
				}
			});

			ArrayList<String> allGestureFileNameList = new ArrayList<String>();
			for (String gestureName : gestureFileNames)
			{
				allGestureFileNameList.add(folderPath + gestureName);
			}
			
			Date now = new Date();
			writeLineToFile(grandParentPath, "AllGestureFileNames-" + parentFolderPath.substring(0, parentFolderPath.length() - 1) + "-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".filenames", toString(allGestureFileNameList.toArray(new String[allGestureFileNameList.size()]), ","), true);
			System.out.println("Wrote All File Names To File.");
		}
	}
	
	public static void readTimeSeriesListsAndGenerateGestures(boolean remap)
	{
		readTimeSeriesListsAndGenerateGestures(originalTimeSeriesListName, originalGesturePath, generatedTimeSeriesListName, generatedGesturePath, remap);
	}
	
	public static void readTimeSeriesListsAndGenerateGestures(String originalTimeSeriesListName, String originalGesturePath, String generatedTimeSeriesListName, String generatedGesturePath, boolean remap)
	{
		readTimeSeriesListAndGenerateGestures(originalTimeSeriesListName, originalGesturePath, remap);
		readTimeSeriesListAndGenerateGestures(generatedTimeSeriesListName, generatedGesturePath, remap);
	}
	
	public static void readTimeSeriesListAndGenerateGestures(String fileName, String folder, boolean remap)
	{
		String grandParentPath = new File(".").getAbsolutePath();
		grandParentPath = grandParentPath.substring(0, grandParentPath.length() - 2);
		String timeSeriesListPath = grandParentPath + grandParentFolderPath + deepIMAGINATIONTimeSeriesPath + fileName;
		BufferedReader br = null;
		
		ArrayList<double[]> timeSeriesList = new ArrayList<double[]>();
		
		try 
		{
			br = new BufferedReader(new FileReader(new File(timeSeriesListPath)));
			
			while(true)
			{
				String line = br.readLine();
				if(line == null)
				{
					break;
				}
				
				String[] lineStrings = line.split(",");
				double[] timeseries = new double[lineStrings.length];
				for(int i = 0; i < lineStrings.length; i++)
				{
					timeseries[i] = Double.parseDouble(lineStrings[i]);
				}
				timeSeriesList.add(timeseries);
			}
			
			String objectName = propNames[(new Random()).nextInt(propNames.length)];
			for(int i = 0; i < propNames.length; i++)
			{
				String propName = propNames[i]; 
				if(fileName.contains(propName))
				{
					objectName = propName;
				}
			}
			
			int count = 1;
			for(double[] timeseries : timeSeriesList)
			{
				
				String objectSize = "SMALL";
				Vector3 objectPosition = new Vector3();
				Quaternion objectRotation = new Quaternion();
				ArrayList<PlayerState> playerStates = new ArrayList<>();
				long time = System.currentTimeMillis();
				Date now = new Date(time);
				
				for(int i = 0; i < timeseries.length; i += 28)
				{
					PlayerState state = new PlayerState();
					state.timestamp = new Date(time + Math.round(i / 28f) * millisecondsPerFrame);
					//For each time point, head, lefthand, righthand, Pelvis/Skeletal[1]. Repeated for Position and Orientation. I.E. [3, 3, 3, 3, 4, 4, 4, 4]
					if(remap)
					{
						state.headPosition = new Vector3(map(timeseries[i + 0], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 1], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 2], 0.0, 1.0, -2.0, 2.0));
						state.leftHandPosition = new Vector3(map(timeseries[i + 3], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 4], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 5], 0.0, 1.0, -2.0, 2.0));
						state.rightHandPosition = new Vector3(map(timeseries[i + 6], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 7], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 8], 0.0, 1.0, -2.0, 2.0));
						state.skeletalPositions[1] = new Vector3(map(timeseries[i + 9], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 10], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 11], 0.0, 1.0, -2.0, 2.0));
						state.headRotation = new Quaternion(map(timeseries[i + 12], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 13], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 14], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 15], 0.0, 1.0, -2.0, 2.0));
						state.leftHandRotation = new Quaternion(map(timeseries[i + 16], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 17], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 18], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 19], 0.0, 1.0, -2.0, 2.0));
						state.rightHandRotation = new Quaternion(map(timeseries[i + 20], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 21], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 22], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 23], 0.0, 1.0, -2.0, 2.0));
						state.skeletalRotations[1] = new Quaternion(map(timeseries[i + 24], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 25], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 26], 0.0, 1.0, -2.0, 2.0), map(timeseries[i + 27], 0.0, 1.0, -2.0, 2.0));
					}
					else
					{
						state.headPosition = new Vector3(timeseries[i + 0], timeseries[i + 1], timeseries[i + 2]);
						state.leftHandPosition = new Vector3(timeseries[i + 3], timeseries[i + 4], timeseries[i + 5]);
						state.rightHandPosition = new Vector3(timeseries[i + 6], timeseries[i + 7], timeseries[i + 8]);
						state.skeletalPositions[1] = new Vector3(timeseries[i + 9], timeseries[i + 10], timeseries[i + 11]);
						state.headRotation = new Quaternion(timeseries[i + 12], timeseries[i + 13], timeseries[i + 14], timeseries[i + 15]);
						state.leftHandRotation = new Quaternion(timeseries[i + 16], timeseries[i + 17], timeseries[i + 18], timeseries[i + 19]);
						state.rightHandRotation = new Quaternion(timeseries[i + 20], timeseries[i + 21], timeseries[i + 22], timeseries[i + 23]);
						state.skeletalRotations[1] = new Quaternion(timeseries[i + 24], timeseries[i + 25], timeseries[i + 26], timeseries[i + 27]);
					}
					playerStates.add(state);
				}
				
				Gesture gesture = new Gesture(UUID.randomUUID(), now, objectName, objectSize, objectPosition, objectRotation, playerStates);
				String generatedFilePath = grandParentPath + grandParentFolderPath + deepIMAGINATIONTimeSeriesPath + folder;
				(new File(generatedFilePath)).mkdirs();
				writeLineToFile(generatedFilePath, (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".gesture", toString(gesture, "$", "^"), true);
				System.out.println("Wrote Gesture To File. " + (count++ * 100f / timeSeriesList.size()) + "% Complete.");
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static String toString(Gesture gesture, String elementSeparator, String stateSeparator)
	{
		String result = gesture.ID.toString() + elementSeparator
				+ (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(gesture.timestamp) + elementSeparator
				+ gesture.objectName + elementSeparator
				+ gesture.objectSize + elementSeparator
				+ gesture.objectPosition.toString() + elementSeparator
				+ gesture.objectRotation.toString() + elementSeparator
				+ toString(gesture.playerStates, stateSeparator);
		
		return result;
	}
	
	public static String toString(ArrayList<PlayerState> states, String stateSeparator)
	{
		String result = "";
		for(PlayerState state : states)
		{
			String value = "{\"timestamp\":\"" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(state.timestamp) + "\","
					+ "\"headPosition\":" + state.headPosition.toString() + ","
					+ "\"headRotation\":" + state.headRotation.toString() + ","
					+ "\"leftHandPosition\":" + state.leftHandPosition.toString() + ","
					+ "\"leftHandRotation\":" + state.leftHandRotation.toString() + ","
					+ "\"rightHandPosition\":" + state.rightHandPosition.toString() + ","
					+ "\"rightHandRotation\":" + state.rightHandRotation.toString() + ","
					+ "\"leftControllerGripButtonDown\":" + state.leftControllerGripButtonDown.toString() + ","
					+ "\"leftControllerTriggerButtonDown\":" + state.leftControllerTriggerButtonDown.toString() + ","
					+ "\"rightControllerGripButtonDown\":" + state.rightControllerGripButtonDown.toString() + ","
					+ "\"rightControllerTriggerButtonDown\":" + state.rightControllerTriggerButtonDown.toString() + ","
					+ "\"leftControllerGripButtonUp\":" + state.leftControllerGripButtonUp.toString() + ","
					+ "\"leftControllerTriggerButtonUp\":" + state.leftControllerTriggerButtonUp.toString() + ","
					+ "\"rightControllerGripButtonUp\":" + state.rightControllerGripButtonUp.toString() + ","
					+ "\"rightControllerTriggerButtonUp\":" + state.rightControllerTriggerButtonUp.toString() + ","
					+ "\"skeletalPositions\":[";
			for(Vector3 position : state.skeletalPositions)
			{
				value += position.toString() + ",";
			}
			value = value.substring(0, value.length() - 1) + "],";
			value += "\"skeletalRotations\":[";
			for(Vector3 position : state.skeletalPositions)
			{
				value += position.toString() + ",";
			}
			value = value.substring(0, value.length() - 1) + "]}";
			
			result += value + stateSeparator;
		}
		result = result.substring(0, result.length() - stateSeparator.length());
		
		return result;
	}
	
	public static void readImageCSVsAndGenerateTimeSeries(boolean isHilbert)
	{
		String grandParentPath = new File(".").getAbsolutePath();
		grandParentPath = grandParentPath.substring(0, grandParentPath.length() - 2);
		ArrayList<ArrayList<String>> propNameOriginalTimeSeriesFileName = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> propNameGeneratedTimeSeriesFileName = new ArrayList<ArrayList<String>>();
		
		for(int i = 0; i < propNames.length; i++)
		{
			propNameOriginalTimeSeriesFileName.add(new ArrayList<String>());
			propNameGeneratedTimeSeriesFileName.add(new ArrayList<String>());
		}
		
		String folderPath = grandParentPath + grandParentFolderPath + imageCSVFolder;
		File folder = new File(folderPath);
		gestureFileNames = folder.list(new FilenameFilter()
		{
			@Override
			public boolean accept(File dir, String name)
			{
				return name.endsWith(".csv");
			}
		});
		
		for(String fileName : gestureFileNames)
		{
			if(fileName.contains("original"))
			{
				originalImageFileNames.add(folderPath + fileName);
				for(int i = 0; i < propNames.length; i++)
				{
					String propName = propNames[i]; 
					if(fileName.contains(propName))
					{
						propNameOriginalTimeSeriesFileName.get(i).add(folderPath + fileName);
					}
				}
			}
			else if(fileName.contains("vae"))
			{
				generatedImageFileNames.add(folderPath + fileName);
				for(int i = 0; i < propNames.length; i++)
				{
					String propName = propNames[i]; 
					if(fileName.contains(propName))
					{
						propNameGeneratedTimeSeriesFileName.get(i).add(folderPath + fileName);
					}
				}
			}
		}
		
		int originallength = originalImageFileNames.size();
		int generatedLength = generatedImageFileNames.size();
		
		System.out.println("Original Files Number: " + originallength);
		System.out.println("Generated Files Number: " + generatedLength);
		
		double[][] originalTimeSeriesArray = new double[originallength][imageCSVRows * imageCSVcolumns - imageCSVpadding];
		double[][] generatedTimeSeriesArray = new double[generatedLength][imageCSVRows * imageCSVcolumns - imageCSVpadding];
		
		int timeSeriesArrayIndex = 0;
		for(String fileName : originalImageFileNames)
		{
			originalTimeSeriesArray[timeSeriesArrayIndex++] = readCSVAndGenerateTimeSeries(fileName, imageCSVRows * imageCSVcolumns - imageCSVpadding, isHilbert);
		}
		System.out.println("Original Files Converted To TimeSeries");
		timeSeriesArrayIndex = 0;
		for(String fileName : generatedImageFileNames)
		{
			generatedTimeSeriesArray[timeSeriesArrayIndex++] = readCSVAndGenerateTimeSeries(fileName, imageCSVRows * imageCSVcolumns - imageCSVpadding, isHilbert);
		}
		System.out.println("Generated Files Converted To TimeSeries");
		
		Date now = new Date();
		for(int i = 0; i < propNames.length; i++)
		{
			originalImageFileNames = propNameOriginalTimeSeriesFileName.get(i);
			originallength = originalImageFileNames.size();
			generatedImageFileNames = propNameGeneratedTimeSeriesFileName.get(i);
			generatedLength = generatedImageFileNames.size();
			double[][] propNameOriginalTimeSeriesArray = new double[originallength][imageCSVRows * imageCSVcolumns - imageCSVpadding];
			double[][] propNameGeneratedTimeSeriesArray = new double[generatedLength][imageCSVRows * imageCSVcolumns - imageCSVpadding];
			
			timeSeriesArrayIndex = 0;
			for(String fileName : originalImageFileNames)
			{
				propNameOriginalTimeSeriesArray[timeSeriesArrayIndex++] = readCSVAndGenerateTimeSeries(fileName, imageCSVRows * imageCSVcolumns - imageCSVpadding, isHilbert);
			}
			System.out.println("Original " + propNames[i] + " Files Converted To TimeSeries");
			timeSeriesArrayIndex = 0;
			for(String fileName : generatedImageFileNames)
			{
				propNameGeneratedTimeSeriesArray[timeSeriesArrayIndex++] = readCSVAndGenerateTimeSeries(fileName, imageCSVRows * imageCSVcolumns - imageCSVpadding, isHilbert);
			}
			System.out.println("Generated " + propNames[i] + " Files Converted To TimeSeries");
			
			writeMatrixToFile(grandParentPath + grandParentFolderPath, "CleanedTimeSeriesListOriginal-" + i + "-" + propNames[i] + "-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".timeseries", propNameOriginalTimeSeriesArray, "\n", ",");
			System.out.println("Wrote Original Time Series To File.");
			writeMatrixToFile(grandParentPath + grandParentFolderPath, "CleanedTimeSeriesListGenerated-" + i + "-" + propNames[i] + "-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".timeseries", propNameGeneratedTimeSeriesArray, "\n", ",");
			System.out.println("Wrote Generated Time Series To File.");
		}
		
//		writeLineToFile(grandParentPath + grandParentFolderPath, "CleanedTimeSeriesListOriginal-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".timeseries", toString(originalTimeSeriesArray, "\n", ","), true);
//		System.out.println("Wrote Time Series To File.");
//		writeLineToFile(grandParentPath + grandParentFolderPath, "CleanedTimeSeriesListGenerated-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".timeseries", toString(generatedTimeSeriesArray, "\n", ","), true);
//		System.out.println("Wrote Time Series To File.");
		writeMatrixToFile(grandParentPath + grandParentFolderPath, "CleanedTimeSeriesListOriginal-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".timeseries", originalTimeSeriesArray, "\n", ",");
		System.out.println("Wrote Original Time Series To File.");
		writeMatrixToFile(grandParentPath + grandParentFolderPath, "CleanedTimeSeriesListGenerated-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".timeseries", generatedTimeSeriesArray, "\n", ",");
		System.out.println("Wrote Generated Time Series To File.");
	}
	
	public static double[] readCSVAndGenerateTimeSeries(String filename, int length, boolean isHilbert)
	{
		double[] timeseries = new double[length];
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new FileReader(new File(filename)));
			int index = 0;
			String line;
			while(true)
			{
				if(index >= length)
				{
					break;
				}
				line = br.readLine();
				if(line == null)
				{
					break;
				}
				String[] doubleStrings = line.split(",");
				for(String doubleString : doubleStrings)
				{
					if(index >= length)
					{
						break;
					}
					timeseries[index++] = Double.parseDouble(doubleString);
				}
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(br != null)
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
				br = null;
			}
		}
		
		if(isHilbert)
		{
			double[] timeseriesHilbert = new double[timeseries.length];
			int size = timeseries.length;
			for(int i = 0; i < size; i++)
			{
				int[] xy = HilbertCurveCoordinateConversionUtils.indexToCoordinate(size, i);
				
				timeseriesHilbert[i] = timeseries[getRowMajorCoordinate(xy, 64, 64)];
			}
			timeseries = timeseriesHilbert;
		}
		
		return timeseries;
	}
	
	public static void readTimeSeriesListAndGenerateImages(boolean isHilbert, boolean areImagesSaved, int rows, int columns)
	{
		int gestureCount = 0;
		String grandParentPath = new File(".").getAbsolutePath();
		grandParentPath = grandParentPath.substring(0, grandParentPath.length() - 2);
		
		File cleanedTimeSeriesListFileNamePath = new File(grandParentPath + grandParentFolderPath + cleanedTimeSeriesListFileName);
		
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new FileReader(cleanedTimeSeriesListFileNamePath));
			String line;
			ArrayList<Object> tsList = new ArrayList<Object>();
			while(gestureCount < imageSetSize)
			{
				try
				{
					if((line = br.readLine()) == null)
					{
						if(!isImageGeneration)
						{
							break;
						}
						br.close();
						br = new BufferedReader(new FileReader(cleanedTimeSeriesListFileNamePath));
						if((line = br.readLine()) == null)
						{
							break;
						}
					}
					String[] valueStrings = line.split(","); 
					int size = valueStrings.length;
					double[] timeseries = new double[isPaddedToSquare ? getCeilingPerfectSquare(size, false) : size];
					double[] timeseriesHilbert = new double[isPaddedToSquare ? getCeilingPerfectSquare(size, false) : size];
					byte[] byteseries = null;
					if(areImagesSaved)
					{
						byteseries = new byte[isPaddedToSquare ? getCeilingPerfectSquare(size, false) : size];
					}
					
					Img<UnsignedByteType> img = null;
					RandomAccess<UnsignedByteType> accessor = null;
					if(areImagesSaved)
					{
						img = new ArrayImgFactory<UnsignedByteType>().create(new long[] { rows, columns }, new UnsignedByteType());
						accessor = img.randomAccess();
					}
					
					for(int i = 0; i < size; i++)
					{
						timeseries[i] = Double.parseDouble(valueStrings[i]);
						
						
						if(isHilbert)
						{
							int[] xy = HilbertCurveCoordinateConversionUtils.indexToCoordinate(size, i);
							timeseriesHilbert[getRowMajorCoordinate(xy, 64, 64)] = map(timeseries[i], -2.0, 2.0, 0.0, 1.0);
							if(areImagesSaved)
							{
								accessor.setPosition(xy);
								byte pixel = (byte) Math.round(map(timeseries[i], -2.0, 2.0, -128, 127));
								byteseries[i] = pixel;
								accessor.get().set(new UnsignedByteType(pixel));
							}
						}
						else
						{
							timeseries[i] = map(timeseries[i], -2.0, 2.0, 0.0, 1.0);
							if(areImagesSaved)
							{
								accessor.setPosition(getRowMajorXYCoordinates(i, rows, columns));
								byte pixel = (byte) Math.round(map(timeseries[i], -2.0, 2.0, -128, 127));
								byteseries[i] = pixel;
								accessor.get().set(new UnsignedByteType(pixel));
							}
						}
					}
					
//					printMinMax(timeseries);
//					
//					printMinMax(byteseries);
					
					if(!isHilbert)
					{
						tsList.add(timeseries);
					}
					else
					{
						tsList.add(timeseriesHilbert);
					}
					
					try
					{
						if(areImagesSaved)
						{
							ImgLib.save(img, grandParentPath + grandParentFolderPath + generatedImageFolder + gestureCount + ".png");
						}
						
						gestureCount++;
					}
					catch (ImgLibException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			double[][] tsListMatrix = new double[tsList.size()][isPaddedToSquare ? getCeilingPerfectSquare(((double[])tsList.get(0)).length, true) : ((double[])tsList.get(0)).length];
			tsList.toArray(tsListMatrix);
			Date now = new Date();
//			writeLineToFile(grandParentPath + grandParentFolderPath, "CleanedTimeSeriesListHilbert-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".timeseries", toString(tsListMatrix, "\n", ","), true);
			if(isHilbert)
			{
				writeMatrixToFile(grandParentPath + grandParentFolderPath, "CleanedTimeSeriesListHilbert-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".timeseries", tsListMatrix, "\n", ",");
				System.out.println("Wrote Time Series Hilbert To File.");
			}
			else
			{
				writeMatrixToFile(grandParentPath + grandParentFolderPath, "CleanedTimeSeriesList-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".timeseries", tsListMatrix, "\n", ",");
				System.out.println("Wrote Time Series To File.");
			}
		}
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			if(br != null)
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				finally
				{
					br = null;
				}
			}
		}
	}
	
	public static int getRowMajorCoordinate(int[] xy, int rows, int columns)
	{
		return xy[0] + xy[1] * rows;
	}
	
	public static int[] getRowMajorXYCoordinates(int index, int rows, int columns)
	{
//		return xy[0] + xy[1] * rows;
		int[] xy = new int[2];
		xy[1] = index / rows;
		xy[0] = index - xy[1] * rows;
		return xy;
	}
	
	public static int getCeilingPerfectSquare(int n, boolean debugText)
	{
		if(Math.sqrt(n) == Math.round(Math.sqrt(n)))
		{
			System.out.println(n + " is a perfect square already.");
			return n;
		}
		
		for(int i = n; true; i++)
		{
			if(Math.sqrt(i) == Math.round(Math.sqrt(i)))
			{
				System.out.println("Ceiling perfect square is " + i);
				return i;
			}
		}
	}
	
	public static double map(double val, double xMin, double xMax, double yMin, double yMax)
	{
		return yMin + ((val - xMin) * (yMax - yMin) / (xMax - xMin));
	}
	
	public static void printMinMax(double[] values)
	{
		double[] min = new double[values.length / numberOfSamplesPerGesture];
		double[] max = new double[values.length / numberOfSamplesPerGesture];
		for(int i = 0, j = 0; i < values.length; i++, j++)
		{
			if(i > 0 && i % (values.length / numberOfSamplesPerGesture) == 0)
			{
				j = 0;
			}
			if(i < (values.length / numberOfSamplesPerGesture))
			{
				min[j] = values[i];
				max[j] = values[i];
			}
			else
			{
				if(min[j] > values[i])
				{
					min[j] = values[i];
				}
				if(max[j] < values[i])
				{
					max[j] = values[i];
				}
			}
		}
		int count = 0;
		System.out.print("Double Min: [");
		for(double value : min)
		{
			System.out.print(value + ((count < min.length - 1) ? "," : "]\n"));
			count++;
		}
		count = 0;
		System.out.print("Double Max: [");
		for(double value : max)
		{
			System.out.print(value + ((count < max.length - 1) ? "," : "]\n"));
			count++;
		}
	}
	
	public static void printMinMax(byte[] values)
	{
		byte[] min = new byte[values.length / numberOfSamplesPerGesture];
		byte[] max = new byte[values.length / numberOfSamplesPerGesture];
		for(int i = 0, j = 0; i < values.length; i++, j++)
		{
			if(i > 0 && i % (values.length / numberOfSamplesPerGesture) == 0)
			{
				j = 0;
			}
			if(i < (values.length / numberOfSamplesPerGesture))
			{
				min[j] = values[i];
				max[j] = values[i];
			}
			else
			{
				if(min[j] > values[i])
				{
					min[j] = values[i];
				}
				if(max[j] < values[i])
				{
					max[j] = values[i];
				}
			}
		}
		int count = 0;
		System.out.print("Byte Min: [");
		for(byte value : min)
		{
			System.out.print(value + ((count < min.length - 1) ? "," : "]\n"));
			count++;
		}
		count = 0;
		System.out.print("Byte Max: [");
		for(byte value : max)
		{
			System.out.print(value + ((count < max.length - 1) ? "," : "]\n"));
			count++;
		}
	}
	
	/**
	 * Method to read in all gestures from a file list and process them. Calculate the distance matrix between 
	 * individual gestures in the file list (optional), output optional labels for each gesture (as a comma separated list of 
	 * affordance values, text phrase representing the pretend object, text phrase representing the pretend action), remap the 
	 * values of the timeseries from the original range to a new range, create a dataset that has padding at the end (or create 
	 * a stretched/skewed dataset with same length in time).
	 * @param calculateDistanceMatrix whether to calculate distance matrix or not
	 * @param outputLabels whether to ouput label files or not
	 * @param remap whether to remap the range of values or not
	 * @param isPaddedTimeSeries whether to create a dataset that has zero padding at the end or to stretch/skew gestures 
	 * to be the same length
	 */
	public static void readGesturesFromFileListAndProcess(boolean calculateDistanceMatrix, boolean outputLabels, boolean remap, boolean isPaddedTimeSeries, int padding, String paddingElement, TimeSeriesType timeSeriesType, int numSamplesPerGesture)
	{
		int gestureCount = 1;
		String grandParentPath = new File(".").getAbsolutePath();
		grandParentPath = grandParentPath.substring(0, grandParentPath.length() - 2);
		
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
		gson = gsonBuilder.create();
		
		File cleanedAffordanceFile = new File(grandParentPath + grandParentFolderPath + propAttributesSummariesFileName);
		File cleanedGestureFile = new File(grandParentPath + grandParentFolderPath + cleanedGestureFileNames);
		
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new FileReader(cleanedAffordanceFile));
			String contents = "";
			
			for(int rowCount = 0; (contents = br.readLine()) != null; rowCount++)
			{
				if(rowCount == 0) //Skipping first row since that is field names/headings.
				{
					continue;
				}
				
				String[] fields = contents.split(",");
				String objectName = fields[0];
				HashMap<String, Integer> objectAffordance = new HashMap<String, Integer>();
				for(int fieldIndex = 2; fieldIndex < fields.length; fieldIndex++)
				{
					 int fieldValue = Integer.parseInt(fields[fieldIndex]);
					 String fieldName = affordanceFields[fieldIndex];
					 objectAffordance.put(fieldName, fieldValue);
				}
				objectAffordances.put(objectName, objectAffordance);
			}
			
			br.close();
			br = null;
			
			br = new BufferedReader(new FileReader(cleanedGestureFile));
			contents = br.readLine();
			gestureFileNames = contents.split(",");
			
			for (String gestureName : gestureFileNames)
			{
				allGestureFileNameList.add(grandParentPath + gestureName);
				Gesture gesture = readGestureFromFile(grandParentPath, gestureName);
				if (gesture != null && gesture.getDuration() >= gestureDurationLowerBound && gesture.getDuration() <= gestureDurationUpperBound)
				{
					TimeSeries ts = null;
					if(!isPaddedTimeSeries)
					{
						TimeSeries ts1 = generateTimeSeriesFromGestureLinearlySpaced(gesture, true, numberOfSamplesPerGesture);
						TimeSeries ts2 = generateTimeSeriesFromGestureLinearlySpaced(gesture, false, numberOfSamplesPerGesture);
						ts = mergeSameSize(ts1, ts2);
					}
					else
					{
						ts = generateTimeSeriesFromGesturePadded(gesture, timeSeriesType, numSamplesPerGesture);
					}
					if (ts != null)
					{
						timeSeriesList.add(ts);
						cleanedGestureFileNameList.add(grandParentPath + gestureName);
						String keyName = gesture.objectName;
						if(objectNameGestureCount.containsKey(keyName))
						{
							objectNameGestureCount.put(keyName, (objectNameGestureCount.get(keyName)) + 1);
						}
						else
						{
							objectNameGestureCount.put(keyName, 1);
						}
						String keyNameSize = gesture.objectName + "," + gesture.objectSize;
						if(objectNameSizeGestureCount.containsKey(keyNameSize))
						{
							objectNameSizeGestureCount.put(keyNameSize, (objectNameSizeGestureCount.get(keyNameSize)) + 1);
						}
						else
						{
							objectNameSizeGestureCount.put(keyNameSize, 1);
						}
						for(Map.Entry<String, Integer> affordance : objectAffordances.get(keyName).entrySet())
						{
							if(affordanceFeatureGestureCount.containsKey(affordance.getKey()))
							{
								affordanceFeatureGestureCount.put(affordance.getKey(), (affordanceFeatureGestureCount.get(affordance.getKey())) + affordance.getValue());
							}
							else
							{
								affordanceFeatureGestureCount.put(affordance.getKey(), affordance.getValue());
							}
						}
						affordanceList.add(affordanceMapToString(objectAffordances.get(keyName), ",", remap));
						System.out.print(((gestureCount % 20 == 1) ? "\n" : "") + gestureCount++ + ", ");
						
						String objectName = gestureName.split("\\$")[2];
						String actionName = gestureName.split("\\$")[3];
						pretendObjectList.add(objectName);
						pretendActionList.add(actionName);
					}
				}
			}
			
			if(remap)
			{
				timeSeriesList = remapTimeSeriesList(timeSeriesList, -2.0, 2.0, 0.0, 1.0, timeSeriesType);
			}
			
			if(calculateDistanceMatrix)
			{
				int size = timeSeriesList.size();
				double[][] warpDistanceMatrix = new double[size][size];
				DistanceFunction distanceFunction = DistanceFunctionFactory.getDistFnByName(distanceFunctionName);
				long distanceCount = 0;
				double distancePercent = 0;
				for (int i = 0; i < size; i++)
				{
					for (int j = 0; j < size; j++)
					{
						if (i <= j)
						{
							warpDistanceMatrix[i][j] = 0;
							distanceCount++;
							continue;
						}

						final TimeWarpInfo info = com.dtw.FastDTW.getWarpInfoBetween(timeSeriesList.get(i), timeSeriesList.get(j), radius, distanceFunction);
						warpDistanceMatrix[i][j] = info.getDistance();
						distanceCount++;
					}
					distancePercent = ((double)distanceCount)/((double)size*size)*100;
					System.out.println("Percent Completed: " + distancePercent);
				}
				grandParentPath += grandParentFolderPath;
//				System.out.println("\nDistances: ");
//				System.out.println(toString(warpDistanceMatrix, "\n", ", "));
				Date now = new Date();
//				writeLineToFile(grandParentPath, "CleanedTimeSeriesList-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".matrix", toString(timeSeriesList, "\n", ","), true);
				writeTimeSeriesListToFile(grandParentPath, "CleanedTimeSeriesList-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".timeseries", timeSeriesList, padding, paddingElement);
				System.out.println("Wrote Cleaned TimeSeries List To File.");
				writeLineToFile(grandParentPath, "CleanedGestureFileNames-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".filenames", toString(cleanedGestureFileNameList.toArray(new String[cleanedGestureFileNameList.size()]), ","), true);
				System.out.println("Wrote Cleaned File Names To File.");
				writeLineToFile(grandParentPath, "AllGestureFileNames-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".filenames", toString(allGestureFileNameList.toArray(new String[allGestureFileNameList.size()]), ","), true);
				System.out.println("Wrote All File Names To File.");
				writeLineToFile(grandParentPath, "ObjectNameGestureCount-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".counts", toString(objectNameGestureCount, "\n", ","), true);
				System.out.println("Wrote Object Name Gesture Count To File.");
				writeLineToFile(grandParentPath, "ObjectNameSizeGestureCount-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".counts", toString(objectNameSizeGestureCount, "\n", ","), true);
				System.out.println("Wrote Object Name & Size Gesture Count To File.");
				writeLineToFile(grandParentPath, "AffordanceFeatureGestureCount-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".counts", toString(affordanceFeatureGestureCount, "\n", ","), true);
				System.out.println("Wrote Affordance Feature Gesture Count To File.");
				writeLineToFile(grandParentPath, "GestureDTWDistanceMatrix-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".matrix", toString(warpDistanceMatrix, "\n", ","), true);
				System.out.println("Wrote Distance Matrix To File.");
				if(outputLabels)
				{
					writeLineToFile(grandParentPath, "CleanedGestureAffordanceLabels-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".labels", toStringCommaSeparatedNewLineEnded(affordanceList), true);
					System.out.println("Wrote Cleaned Gesture Affordance Labels To File.");
					writeLineToFile(grandParentPath, "CleanedGestureObjectLabels-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".labels", toStringCommaSeparatedNewLineEnded(pretendObjectList), true);
					System.out.println("Wrote Cleaned Gesture Object Labels To File.");
					writeLineToFile(grandParentPath, "CleanedGestureActionLabels-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".labels", toStringCommaSeparatedNewLineEnded(pretendActionList), true);
					System.out.println("Wrote Cleaned Gesture Action Labels To File.");
				}
			}
			else
			{
				grandParentPath += grandParentFolderPath;
				Date now = new Date();
				writeTimeSeriesListToFile(grandParentPath, "CleanedTimeSeriesList-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".timeseries", timeSeriesList, padding, paddingElement);
				System.out.println("Wrote Cleaned TimeSeries List To File.");
				writeLineToFile(grandParentPath, "CleanedGestureFileNames-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".filenames", toString(cleanedGestureFileNameList.toArray(new String[cleanedGestureFileNameList.size()]), ","), true);
				System.out.println("Wrote Cleaned File Names To File.");
				if(outputLabels)
				{
					writeLineToFile(grandParentPath, "CleanedGestureAffordanceLabels-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".labels", toStringCommaSeparatedNewLineEnded(affordanceList), true);
					System.out.println("Wrote Cleaned Gesture Affordance Labels To File.");
					writeLineToFile(grandParentPath, "CleanedGestureObjectLabels-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".labels", toStringCommaSeparatedNewLineEnded(pretendObjectList), true);
					System.out.println("Wrote Cleaned Gesture Object Labels To File.");
					writeLineToFile(grandParentPath, "CleanedGestureActionLabels-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".labels", toStringCommaSeparatedNewLineEnded(pretendActionList), true);
					System.out.println("Wrote Cleaned Gesture Action Labels To File.");
				}
			}
		}
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			if(br != null)
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				finally
				{
					br = null;
				}
			}
		}
	}
	
	public static ArrayList<TimeSeries> remapTimeSeriesList(ArrayList<TimeSeries> timeSeriesList, double xMin, double xMax, double yMin, double yMax, TimeSeriesType type)
	{
		ArrayList<TimeSeries> result = new ArrayList<TimeSeries>();
		
		for(TimeSeries ts : timeSeriesList)
		{
			TimeSeries tsNew = new TimeSeries(ts.numOfDimensions());
			for(int i = 0; i < ts.size(); i++)
			{
				double[] measurements = ts.getMeasurementVector(i);
				boolean isPadding = true;
				for(int j = 0; j < measurements.length; j++)
				{
					if(measurements[j] != 0)
					{
						isPadding = false;
						break;
					}
				}
				for(int j = 0; !isPadding && j < measurements.length; j++)
				{
					if(type == TimeSeriesType.BOTHWITHGRABS30 && j > 27 && j < 30)
					{
						continue;
					}
					measurements[j] = map(measurements[j], xMin, xMax, yMin, yMax);
				}
				tsNew.addLast(i, new TimeSeriesPoint(measurements));
			}
			result.add(tsNew);
		}
		
		return result;
	}
	
	public static String toStringCommaSeparatedNewLineEnded(ArrayList<String> strings)
	{
		String result = "";
		
		for(String string : strings)
		{
			result += string + "\n";
			
		}
		
		return result.substring(0, result.length() - 1);
	}
	
	public static String affordanceMapToString(HashMap<String, Integer> affordances, String elementSeparator, boolean remap)
	{
		String result = "";
		
		for(int fieldIndex = 2; fieldIndex < affordanceFields.length; fieldIndex++)
		{
			double value = 0.0;
			if(remap)
			{
				value = map(affordances.get(affordanceFields[fieldIndex]) * 1.0, 0.0, 3.0, 0.0, 1.0);
			}
			else
			{
				value = affordances.get(affordanceFields[fieldIndex]);
			}
			result += value + elementSeparator;
		}
		
		return result.substring(0, result.length() - elementSeparator.length());
	}
	
	public static TimeSeries mergeSameSize(TimeSeries ts1, TimeSeries ts2)
	{
		TimeSeries ts = new TimeSeries(ts1.numOfDimensions() + ts2.numOfDimensions());
		int size = ts1.size();
		for (int i = 0; i < size; i++)
		{
			double[] values = new double[ts1.numOfDimensions() + ts2.numOfDimensions()];
			TimeSeriesPoint tsp1 = ts1.getMeasurementPoint(i);
			TimeSeriesPoint tsp2 = ts2.getMeasurementPoint(i);
			for(int j = 0; j < values.length; j++)
			{
				if(j < ts1.numOfDimensions())
				{
					values[j] = tsp1.get(j);
				}
				else
				{
					values[j] = tsp2.get(j - ts1.numOfDimensions());
				}
			}
			TimeSeriesPoint tsp = new TimeSeriesPoint(values);
			ts.addLast(i, tsp);
		}
		
		return ts; 
	}
	
	/**
	 * Method to iterate through a set of folders and read all files before filtering them (optionally) and 
	 * adding them to a comma eparated list of filenames.
	 */
	public static void readFoldersAndCreateFileList()
	{
		String grandParentPath = new File(".").getAbsolutePath();
		grandParentPath = grandParentPath.substring(0, grandParentPath.length() - 2) + grandParentFolderPath;

		ArrayList<String> allGestureFileNameList = new ArrayList<String>();
		String folderPath = "";
		for (String parentFolderPath : parentAnnotatedFolderPaths)
		{
			System.out.println("Folder: " + parentFolderPath);
			folderPath = grandParentPath + parentFolderPath;
			File folder = new File(folderPath);
			String[] gestureFileNames = folder.list(new FilenameFilter()
			{
				@Override
				public boolean accept(File dir, String name)
				{
					return name.endsWith(".gesture") && (name.contains("$") || !isAnnotatedGestureOnly);
				}
			});
			
			for (String gestureName : gestureFileNames)
			{
				allGestureFileNameList.add(grandParentFolderPath + parentFolderPath + gestureName);
			}
		}
		
		Date now = new Date();
		writeLineToFile(grandParentPath, "AllGestureFileNamesRelative-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".filenames", toString(allGestureFileNameList.toArray(new String[allGestureFileNameList.size()]), ","), true);
		System.out.println("Wrote All File Names To File.");
	}
	
	public static void readFilesAndProcess()
	{
		int gestureCount = 1;
		String grandParentPath = new File(".").getAbsolutePath();
		grandParentPath = grandParentPath.substring(0, grandParentPath.length() - 2) + grandParentFolderPath;

		String folderPath = "";
		for (String parentFolderPath : parentFolderPaths)
		{
			System.out.print("\tFolder: " + parentFolderPath + "\t");
			folderPath = grandParentPath + parentFolderPath;
			File folder = new File(folderPath);
			gestureFileNames = folder.list(new FilenameFilter()
			{
				@Override
				public boolean accept(File dir, String name)
				{
					return name.endsWith(".gesture");
				}
			});

			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
			gson = gsonBuilder.create();
			
			for (String gestureName : gestureFileNames)
			{
				allGestureFileNameList.add(folderPath + gestureName);
				Gesture gesture = readGestureFromFile(folderPath, gestureName);
				if (gesture != null && gesture.getDuration() >= gestureDurationLowerBound && gesture.getDuration() <= gestureDurationUpperBound)
				{
					// COMMENTED OUT SO ALL GESTURES AREN'T CACHED ON READ
					// THROUGH.
					// gestureList.add(gesture);
					TimeSeries ts = generateTimeSeriesFromGestureLinearlySpaced(gesture, true, numberOfSamplesPerGesture);
					if (ts != null)
					{
						timeSeriesList.add(ts);
						cleanedGestureFileNameList.add(folderPath + gestureName);
						String keyName = gesture.objectName;
						if(objectNameGestureCount.containsKey(keyName))
						{
							objectNameGestureCount.put(keyName, (objectNameGestureCount.get(keyName)) + 1);
						}
						else
						{
							objectNameGestureCount.put(keyName, 1);
						}
						String keyNameSize = gesture.objectName + "," + gesture.objectSize;
						if(objectNameSizeGestureCount.containsKey(keyNameSize))
						{
							objectNameSizeGestureCount.put(keyNameSize, (objectNameSizeGestureCount.get(keyNameSize)) + 1);
						}
						else
						{
							objectNameSizeGestureCount.put(keyNameSize, 1);
						}
						System.out.print(((gestureCount % 20 == 1) ? "\n" : "") + gestureCount++ + ", ");
					}
				}
			}
		}

		int size = timeSeriesList.size();
		double[][] warpDistanceMatrix = new double[size][size];
		DistanceFunction distanceFunction = DistanceFunctionFactory.getDistFnByName(distanceFunctionName);
		long distanceCount = 0;
		double distancePercent = 0;
		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				if (i <= j)
				{
					warpDistanceMatrix[i][j] = 0;
					distanceCount++;
					continue;
				}

				final TimeWarpInfo info = com.dtw.FastDTW.getWarpInfoBetween(timeSeriesList.get(i), timeSeriesList.get(j), radius, distanceFunction);
				warpDistanceMatrix[i][j] = info.getDistance();
				distanceCount++;
			}
			distancePercent = ((double)distanceCount)/((double)size*size)*100;
			System.out.println("Percent Completed: " + distancePercent);
		}

//		System.out.println("\nDistances: ");
//		System.out.println(toString(warpDistanceMatrix, "\n", ", "));
		Date now = new Date();
//		writeLineToFile(grandParentPath, "CleanedTimeSeriesList-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".matrix", toString(timeSeriesList, "\n", ","), true);
		writeTimeSeriesListToFile(grandParentPath, "CleanedTimeSeriesList-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".timeseries", timeSeriesList, 0, "");
		System.out.println("Wrote Cleaned TimeSeries List To File.");
		writeLineToFile(grandParentPath, "CleanedGestureFileNames-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".filenames", toString(cleanedGestureFileNameList.toArray(new String[cleanedGestureFileNameList.size()]), ","), true);
		System.out.println("Wrote Cleaned File Names To File.");
		writeLineToFile(grandParentPath, "AllGestureFileNames-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".filenames", toString(allGestureFileNameList.toArray(new String[allGestureFileNameList.size()]), ","), true);
		System.out.println("Wrote All File Names To File.");
		writeLineToFile(grandParentPath, "ObjectNameGestureCount-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".counts", toString(objectNameGestureCount, "\n", ","), true);
		System.out.println("Wrote Object Name Gesture Count To File.");
		writeLineToFile(grandParentPath, "ObjectNameSizeGestureCount-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".counts", toString(objectNameSizeGestureCount, "\n", ","), true);
		System.out.println("Wrote Object Name & Size Gesture Count To File.");
		writeLineToFile(grandParentPath, "GestureDTWDistanceMatrix-" + numberOfSamplesPerGesture + "-" + gestureDurationLowerBound + "-" + gestureDurationUpperBound + "-" + (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS")).format(now) + ".matrix", toString(warpDistanceMatrix, "\n", ","), true);
		System.out.println("Wrote Distance Matrix To File.");
	}

	/**
	 * Method to write a line of text to a file.
	 * @param folderPath The folder/directory path to the file
	 * @param fileName The name of the file to be written
	 * @param line The line of text to write
	 * @param append Whether to append the line of text to the file or overwrite it
	 */
	public static void writeLineToFile(String folderPath, String fileName, String line, boolean append)
	{
		BufferedWriter bw = null;
		try
		{
			bw = new BufferedWriter(new FileWriter(new File(folderPath + fileName), append));
			bw.write(line);
			bw.close();
			bw = null;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (bw != null)
			{
				try
				{
					bw.close();
				}
				catch (IOException e1)
				{
					e1.printStackTrace();
				}
			}
		}
	}
	
	public static void writeMatrixToFile(String folderPath, String gestureName, double[][] arrayofArrays, String separatorArrays, String separatorElements)
	{
		BufferedWriter bw = null;
		try
		{
			bw = new BufferedWriter(new FileWriter(new File(folderPath + gestureName), true));
			bw.write("");
			for(int i = 0; i < arrayofArrays.length; i++)
			{
				String timeseries = toString(arrayofArrays[i], separatorElements);
				bw.write(timeseries + ((i < arrayofArrays.length - 1) ? separatorArrays : ""));
				System.out.println("Wrote Array To File: " + ((i+1) * 100f / arrayofArrays.length));
			}
			bw.write("");
			bw.close();
			bw = null;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (bw != null)
			{
				try
				{
					bw.close();
				}
				catch (IOException e1)
				{
					e1.printStackTrace();
				}
			}
		}
	}
	
	public static void writeTimeSeriesListToFile(String folderPath, String gestureName, ArrayList<TimeSeries> timeSeriesList, int padding, String paddingElement)
	{
		BufferedWriter bw = null;
		try
		{
			bw = new BufferedWriter(new FileWriter(new File(folderPath + gestureName), true));
			bw.write("");
			for(int i = 0; i < timeSeriesList.size(); i++)
			{
				TimeSeries ts = timeSeriesList.get(i);
				bw.write(toString(ts, ",", padding, paddingElement) + ((i < timeSeriesList.size() - 1) ? "\n" : ""));
				System.out.println("Wrote Timeseries To File: " + ((i+1) * 100f / timeSeriesList.size()));
			}
			bw.write("");
			bw.close();
			bw = null;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (bw != null)
			{
				try
				{
					bw.close();
				}
				catch (IOException e1)
				{
					e1.printStackTrace();
				}
			}
		}
	}
	
	public static String toString(HashMap<String, Integer> dictionary, String separatorElements, String separatorPairs)
	{
		String value = "";
		for (Map.Entry<String, Integer> entry : dictionary.entrySet())
		{
			value += "";
			
			value += entry.getKey() + separatorPairs + entry.getValue() + "" + separatorElements;
		}
		return value.substring(0, value.length() - separatorElements.length()) + "";
	}
	
	public static String toString(ArrayList<TimeSeries> timeSeriesList, String separatorSeries, String separatorElements)
	{
		String value = "";
		for (TimeSeries ts : timeSeriesList)
		{
			value += "";
			for(int i = 0; i < ts.size(); i++)
			{
				double[] measurements = ts.getMeasurementVector(i);
				for(double measurement : measurements)
				{
					value += measurement + separatorElements;
				}
			}
			value += value.substring(0, value.length() - separatorElements.length()) + "" + separatorSeries;
		}
		return value.substring(0, value.length() - separatorSeries.length()) + "";
	}
	
	public static String toString(TimeSeries ts, String separatorElements, int padding, String paddingElement)
	{
		String value = "";
		
		for(int i = 0; i < ts.size(); i++)
		{
			double[] measurements = ts.getMeasurementVector(i);
			for(double measurement : measurements)
			{
				value += measurement + separatorElements;
			}
		}
		for(int i = 0; i < padding; i++)
		{
			value += paddingElement + separatorElements;
		}
		return value.substring(0, value.length() - separatorElements.length()) + "";
	}

	public static String toString(double[][] arrayofArrays, String separatorArrays, String separatorElements)
	{
		String value = "";
		for (double[] array : arrayofArrays)
		{
			String result = "";
			for (double element : array)
			{
				result += element + separatorElements;
			}
			value += result.substring(0, result.length() - separatorElements.length()) + "" + separatorArrays;
		}
		return value.substring(0, value.length() - separatorArrays.length()) + "";
	}
	
	public static String toString(double[] array, String separatorElements)
	{
		String value = "";
		for (double element : array)
		{
			value += element + separatorElements;
		}
		return value.substring(0, value.length() - separatorElements.length()) + "";
	}
	
	public static String toString(String[] array, String separatorElements)
	{
		String value = "";
		for (String element : array)
		{
			value += element + separatorElements;
		}
		return value.substring(0, value.length() - separatorElements.length()) + "";
	}
	
	public static TimeSeries generateTimeSeriesFromGesturePadded(Gesture gesture, TimeSeriesType type, int numberOfSamplesPerGesture)
	{
		TimeSeries ts = null;
		int dimensions;
		switch(type)
		{
			case POSITION12:
				break;
			case ORIENTATION16:
				break;
			case BOTH28:
				break;
			case BOTHWITHGRABS30:
				dimensions = 30;
				ts = new TimeSeries(dimensions);
				boolean leftGrabbed = false;;
				boolean rightGrabbed = false;
				for(int i = 0; i < numberOfSamplesPerGesture; i++)
				{
					double[] measurements = new double[dimensions];
					if(i < gesture.getPlayerStates().size() / 2f)
					{
						PlayerState state = gesture.getPlayerStates().get(i * 2);
						Vector3 headPosition = new Vector3(state.skeletalPositions[5]);
						measurements[0] = headPosition.x;
						measurements[1] = headPosition.y;
						measurements[2] = headPosition.z;
						Quaternion headRotation = new Quaternion(state.skeletalRotations[5]);
						measurements[3] = headRotation.x;
						measurements[4] = headRotation.y;
						measurements[5] = headRotation.z;
						measurements[6] = headRotation.w;
						Vector3 leftHandPosition = new Vector3(state.skeletalPositions[9]);
						measurements[7] = leftHandPosition.x;
						measurements[8] = leftHandPosition.y;
						measurements[9] = leftHandPosition.z;
						Quaternion leftHandRotation = new Quaternion(state.skeletalRotations[9]);
						measurements[10] = leftHandRotation.x;
						measurements[11] = leftHandRotation.y;
						measurements[12] = leftHandRotation.z;
						measurements[13] = leftHandRotation.w;
						Vector3 rightHandPosition = new Vector3(state.skeletalPositions[13]);
						measurements[14] = rightHandPosition.x;
						measurements[15] = rightHandPosition.y;
						measurements[16] = rightHandPosition.z;
						Quaternion rightHandRotation = new Quaternion(state.skeletalRotations[13]);
						measurements[17] = rightHandRotation.x;
						measurements[18] = rightHandRotation.y;
						measurements[19] = rightHandRotation.z;
						measurements[20] = rightHandRotation.w;
						Vector3 pelvisPosition = new Vector3(state.skeletalPositions[1]);
						measurements[21] = pelvisPosition.x;
						measurements[22] = pelvisPosition.y;
						measurements[23] = pelvisPosition.z;
						Quaternion pelvisRotation = new Quaternion(state.skeletalRotations[1]);
						measurements[24] = pelvisRotation.x;
						measurements[25] = pelvisRotation.y;
						measurements[26] = pelvisRotation.z;
						measurements[27] = pelvisRotation.w;
						if(state.getLeftControllerGripButtonDown())
						{
							leftGrabbed = true;
						}
						else if(state.getLeftControllerGripButtonUp())
						{
							leftGrabbed = false;
						}
						measurements[28] = (leftGrabbed) ? 1 : 0;
						if(state.getRightControllerGripButtonDown())
						{
							rightGrabbed = true;
						}
						else if(state.getRightControllerGripButtonUp())
						{
							rightGrabbed = false;
						}
						measurements[29] = (rightGrabbed) ? 1 : 0;
					}
					ts.addLast(i+1, new TimeSeriesPoint(measurements));
				}
				break;
			case FOURJOINTWITHOBJECT:
				dimensions = 35;
				ts = new TimeSeries(dimensions);
				for(int i = 0; i < numberOfSamplesPerGesture; i++)
				{
					double[] measurements = new double[dimensions];
					if(i < gesture.getPlayerStates().size() / 2f)
					{
						PlayerState state = gesture.getPlayerStates().get(i * 2 );
						Vector3 headPosition = new Vector3(state.skeletalPositions[5]);
						measurements[0] = headPosition.x;
						measurements[1] = headPosition.y;
						measurements[2] = headPosition.z;
						Quaternion headRotation = new Quaternion(state.skeletalRotations[5]);
						measurements[3] = headRotation.x;
						measurements[4] = headRotation.y;
						measurements[5] = headRotation.z;
						measurements[6] = headRotation.w;
						Vector3 leftHandPosition = new Vector3(state.skeletalPositions[9]);
						measurements[7] = leftHandPosition.x;
						measurements[8] = leftHandPosition.y;
						measurements[9] = leftHandPosition.z;
						Quaternion leftHandRotation = new Quaternion(state.skeletalRotations[9]);
						measurements[10] = leftHandRotation.x;
						measurements[11] = leftHandRotation.y;
						measurements[12] = leftHandRotation.z;
						measurements[13] = leftHandRotation.w;
						Vector3 rightHandPosition = new Vector3(state.skeletalPositions[13]);
						measurements[14] = rightHandPosition.x;
						measurements[15] = rightHandPosition.y;
						measurements[16] = rightHandPosition.z;
						Quaternion rightHandRotation = new Quaternion(state.skeletalRotations[13]);
						measurements[17] = rightHandRotation.x;
						measurements[18] = rightHandRotation.y;
						measurements[19] = rightHandRotation.z;
						measurements[20] = rightHandRotation.w;
						Vector3 pelvisPosition = new Vector3(state.skeletalPositions[1]);
						measurements[21] = pelvisPosition.x;
						measurements[22] = pelvisPosition.y;
						measurements[23] = pelvisPosition.z;
						Quaternion pelvisRotation = new Quaternion(state.skeletalRotations[1]);
						measurements[24] = pelvisRotation.x;
						measurements[25] = pelvisRotation.y;
						measurements[26] = pelvisRotation.z;
						measurements[27] = pelvisRotation.w;
						Vector3 objectPosition = new Vector3(state.objectPosition);
						measurements[28] = objectPosition.x;
						measurements[29] = objectPosition.y;
						measurements[30] = objectPosition.z;
						Quaternion objectRotation = new Quaternion(state.objectRotation);
						measurements[31] = objectRotation.x;
						measurements[32] = objectRotation.y;
						measurements[33] = objectRotation.z;
						measurements[34] = objectRotation.w;
					}
					ts.addLast(i+1, new TimeSeriesPoint(measurements));
				}
				break;
			default:
		}
		return ts;
	}
	
	public static TimeSeries generateTimeSeriesFromGestureLinearlySpaced(Gesture gesture, boolean isPositionBased, int numberOfSamplesPerGesture)
	{
		TimeSeries ts = null;
		GestureInterpolator interpolator = new GestureInterpolator(gesture.playerStates);
		int length = numberOfSamplesPerGesture - 1;
		if (isPositionBased)
		{
			int dimensions = 4 * 3;
			ts = new TimeSeries(dimensions);
			for (int i = 0; i < length; i++)
			{
				TimeSeriesPoint tsp = getTimeSeriesPointAtFractionVector3(interpolator, (((double) i) / ((double) length)), dimensions);
				ts.addLast(i, tsp);
			}
			TimeSeriesPoint tsp = getTimeSeriesPointAtFractionVector3(interpolator, 1.0, dimensions);
			ts.addLast(length, tsp);
		}
		else
		{
			int dimensions = 4 * 4;
			ts = new TimeSeries(dimensions);
			for (int i = 0; i < length; i++)
			{
				TimeSeriesPoint tsp = getTimeSeriesPointAtFractionQuaternion(interpolator, (((double) i) / ((double) length)), dimensions);
				ts.addLast(i, tsp);
			}
			TimeSeriesPoint tsp = getTimeSeriesPointAtFractionQuaternion(interpolator, 1.0, dimensions);
			ts.addLast(length, tsp);
		}

		return ts;
	}

	private static TimeSeriesPoint getTimeSeriesPointAtFractionVector3(GestureInterpolator interpolator, double fraction, int dimensions)
	{
		//Joints: [root, pelvis, spine, chest, neck, head, leftShoulder, leftUpperArm, leftForearm, leftHand, rightShoulder, rightUpperArm, rightForearm, rightHand, leftThigh, leftCalf, leftFoot, leftToes, rightThigh, rightCalf, rightFoot, rightToes]
		double[] measurements = new double[dimensions];
		PlayerState state = interpolator.atFraction(fraction);
		Vector3 headPosition = new Vector3(state.skeletalPositions[5]);
		measurements[0] = headPosition.x;
		measurements[1] = headPosition.y;
		measurements[2] = headPosition.z;
		Vector3 leftHandPosition = new Vector3(state.skeletalPositions[9]);
		measurements[3] = leftHandPosition.x;
		measurements[4] = leftHandPosition.y;
		measurements[5] = leftHandPosition.z;
		Vector3 rightHandPosition = new Vector3(state.skeletalPositions[13]);
		measurements[6] = rightHandPosition.x;
		measurements[7] = rightHandPosition.y;
		measurements[8] = rightHandPosition.z;
		Vector3 pelvisPosition = new Vector3(state.skeletalPositions[1]);
		measurements[9] = pelvisPosition.x;
		measurements[10] = pelvisPosition.y;
		measurements[11] = pelvisPosition.z;
		return new TimeSeriesPoint(measurements);
	}

	public static TimeSeriesPoint getTimeSeriesPointAtFractionQuaternion(GestureInterpolator interpolator, double fraction, int dimensions)
	{
		//Joints: [root, pelvis, spine, chest, neck, head, leftShoulder, leftUpperArm, leftForearm, leftHand, rightShoulder, rightUpperArm, rightForearm, rightHand, leftThigh, leftCalf, leftFoot, leftToes, rightThigh, rightCalf, rightFoot, rightToes]
		double[] measurements = new double[dimensions];
		PlayerState state = interpolator.atFraction(fraction);
		Quaternion headRotation = new Quaternion(state.skeletalRotations[5]);
		measurements[0] = headRotation.x;
		measurements[1] = headRotation.y;
		measurements[2] = headRotation.z;
		measurements[3] = headRotation.w;
		Quaternion leftHandRotation = new Quaternion(state.skeletalRotations[9]);
		measurements[4] = leftHandRotation.x;
		measurements[5] = leftHandRotation.y;
		measurements[6] = leftHandRotation.z;
		measurements[7] = leftHandRotation.w;
		Quaternion rightHandRotation = new Quaternion(state.skeletalRotations[13]);
		measurements[8] = rightHandRotation.x;
		measurements[9] = rightHandRotation.y;
		measurements[10] = rightHandRotation.z;
		measurements[11] = rightHandRotation.w;
		Quaternion pelvisRotation = new Quaternion(state.skeletalRotations[1]);
		measurements[9] = pelvisRotation.x;
		measurements[10] = pelvisRotation.y;
		measurements[11] = pelvisRotation.z;
		measurements[12] = pelvisRotation.w;
		return new TimeSeriesPoint(measurements);
	}

	public static Gesture readGestureFromFile(String folderPath, String gestureName)
	{
		File gestureFile = new File(folderPath + gestureName);
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new FileReader(gestureFile));
			return parseGesture(br.readLine());
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (br != null)
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	private static int badDateFormatFileCount = 0;
	public static Gesture parseGesture(String gestureContent)
	{
		String[] gestureContents = gestureContent.split("\\$");
		// System.out.println("GestureContents length: " +
		// gestureContents.length);
		String[] playerStates = gestureContents[6].split("\\^");
		Gesture gesture = new Gesture();
		// System.out.println("UUID: " + gestureContents[0]);
		gesture.ID = UUID.fromString(gestureContents[0]);
		try
		{
			gesture.timestamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS").parse(gestureContents[1]);
		}
		catch (ParseException e)
		{
			try
			{
				badDateFormatFileCount++;
				System.out.println("BAD DATE FORMAT COUNT: " + badDateFormatFileCount + ". DATE CONTENT: " + gestureContents[1]);
				gesture.timestamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a").parse(gestureContents[1]);
			}
			catch (ParseException e1)
			{
				e1.printStackTrace();
			}
		}
		gesture.objectName = gestureContents[2];
		gesture.objectSize = gestureContents[3];
		gesture.objectPosition = gson.fromJson(gestureContents[4], Vector3.class);
		gesture.objectRotation = gson.fromJson(gestureContents[5], Quaternion.class);
		for (String stateString : playerStates)
		{
			PlayerState state = gson.fromJson(stateString, PlayerState.class);
			gesture.playerStates.add(state);
		}
		// System.out.println("Gesture: " + gesture.toString());
		return gesture;
	}
}

class DateDeserializer implements JsonDeserializer<Date>
{
	@Override
	public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
	{
		try
		{ // e.g. 2018-01-16-16-00-04-345
			String dateString = json.toString();
			dateString = dateString.substring(1, dateString.length() - 2);
			return new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS").parse(dateString);
		}
		catch (ParseException e)
		{
			e.printStackTrace();
			return null;
		}
	}
}