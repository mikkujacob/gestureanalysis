package adamlab.mime.data.analysis.utils;

public class HilbertCurveCoordinateConversionUtils
{
	// convert (x,y) to d
	public static int coordinateToIndex (int n, int x, int y)
	{
	    int rx, ry, s, d = 0;
	    int[] xy = new int[] {x, y};	    
	    for (s = n / 2; s > 0; s /= 2)
	    {
	        rx = ((xy[0] & s) > 0) ? 1 : 0;
	        ry = ((xy[1] & s) > 0) ? 1 : 0;
	        d += s * s * ((3 * rx) ^ ry);
	        xy = rotate(s, xy[0], xy[1], rx, ry);
	    }
	    return d;
	}

	// convert d to (x,y)
	public static int[] indexToCoordinate(int n, int d)
	{
	    int rx, ry, s, t = d;
	    int[] xy = new int[2];
	    for (s = 1; s < n; s *= 2)
	    {
	        rx = 1 & (t/2);
	        ry = 1 & (t ^ rx);
	        xy = rotate(s, xy[0], xy[1], rx, ry);
	        xy[0] += s * rx;
	        xy[1] += s * ry;
	        t /= 4;
	    }
	    return xy;
	}

	// rotate/flip a quadrant appropriately
	public static int[] rotate(int n, int x, int y, int rx, int ry)
	{
		int[] xy = new int[2];
	    if (ry == 0)
	    {
	        if (rx == 1)
	        {
	            x = n-1 - x;
	            y = n-1 - y;
	        }

	        xy[0] = y;
	        xy[1] = x;
	    }
	    else
	    {
	    		xy[0] = x;
	    		xy[1] = y;
	    }
	    
	    return xy;
	}
	
	public static void main(String[] args)
	{
		int L = 6;
		int points = (int) Math.pow(4, L);
		System.out.println("Hilbert Curve of level L has 4^L points. Currently L = " + L + " and there are " + points + " points");
		int[][] coordinatesFromIndex = new int[points][2];
		int[][] indexFromCoordinates = new int[(int)Math.sqrt(points)][(int)Math.sqrt(points)];
		for(int i = 0; i < points; i++)
		{
			coordinatesFromIndex[i] = indexToCoordinate(points, i);
			System.out.println("Coordinates for index = " + i + ", are " + coordinatesFromIndex[i][0] + "," + coordinatesFromIndex[i][1]);
		}
		
		for(int i = 0; i < (int)Math.sqrt(points); i++)
		{
			for(int j = 0; j < (int)Math.sqrt(points); j++)
			{
				indexFromCoordinates[i][j] = coordinateToIndex(points, i, j);
				System.out.println("Index for coordinates = " + i + ", " + j + ", is " + indexFromCoordinates[i][j]);
			}
		}
	}
}
